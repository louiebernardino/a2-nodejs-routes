//Declare dependencies
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const validator = require("validator")

//Define your schema
const teamSchema = new Schema(
	{
		name: {
			type: String,
			required: true,
			lowercase: true,
			trim: true
		},
		budget: {
			type: Number,
			required: [true, "Budget is required!"],
			validate(value) {
				if(value < 100) {
					throw new Error("Budget must be more than 100!")
				}
			}
		},
		level: {
			type: Number,
			enum: [1, 2, 3],
			default: 1
			// required: [true, "Level is required!"]	
		},
		email: {
			type: String,
			required: true,
			validate(value) {
				//VALIDATION
				// console.log(validator.isEmail(value))
				if(!validator.isEmail(value)) {
					throw new Error("Email is invalid!")
				}

				//SANITATION
				// console.log(validator.normalizeEmail(value, {
				// 	all_lowercase: true
				console.log(this.email = validator.normalizeEmail(value, {
					all_lowercase: true
				}))
			}
		},
		isActive: {
			type: Boolean,
			required: true,
			validate(value) {
				console.log(validator.isBoolean)
				if(validator.isBoolean) {
					console.log("not boolean")
					return (this.isActive = validator.toBoolean(value))
				}
			} 
		}
	},
	{
		timestamps: true
	}
);

//Export your model
module.exports = mongoose.model("Team", teamSchema);