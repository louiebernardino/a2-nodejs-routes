const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//Define your schema
const memberSchema = new Schema(
	{
		name: String,
		position: String
	},
	{
		timestamps: true
	}
);

//Export your model
module.exports = mongoose.model("Member", memberSchema);