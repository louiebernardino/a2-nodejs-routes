//Declare dependencies
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//Define your schema
const taskSchema = new Schema(
	{
		description: String,
		teamId: String,
		isCompleted: Boolean
	},
	{
		timestamps: true
	}
);

//Export your model
module.exports = mongoose.model("Task", taskSchema);