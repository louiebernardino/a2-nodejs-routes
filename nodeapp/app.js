//Declare dependencies
const express = require("express");
const app = express();
const mongoose = require("mongoose");
//----------------------------------------


//Connect to DB
mongoose.connect("mongodb://localhost:27017/mern2_tracker", 
{useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false}); //DeprecationWarning

mongoose.connection.once("open", () => {
	console.log("Now connected to local MongoDB");
});
//-----------------------------------------

//Apply Middleware (auth, validation) Request response pipeline
app.use(express.json());
app.use(express.urlencoded({extended: true}));
//-----------------------------------------

//Declare Models
const Team = require("./models/teams");
const Task = require("./models/tasks");
const Member = require("./models/members");
//-----------------------------------------

//Create Routes/Endpoints
//Transferred Routes
//Declare the resources
const teamsRoute = require("./routes/teams")
app.use("/teams", teamsRoute)
const tasksRoute = require("./routes/tasks")
app.use("/tasks", tasksRoute)
const membersRoute = require("./routes/members")
app.use("/members", membersRoute)

//Initialize the server
app.listen(4031, () => {
	console.log("Now listening to port 4031 :)");
});

